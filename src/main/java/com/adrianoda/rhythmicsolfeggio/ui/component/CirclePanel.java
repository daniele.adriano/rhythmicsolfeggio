package com.adrianoda.rhythmicsolfeggio.ui.component;

import javax.swing.*;
import java.awt.*;

public class CirclePanel extends JPanel {

    private final static Integer RADIUS = 20;
    private final static Color DEFAULT_COLOR = Color.green;
    private final static Color[] PLAY_COLORS = {Color.red, Color.white};

    private Color color;
    private int playIndex = 1;

    public CirclePanel() {
        color = DEFAULT_COLOR;
    }

    /**
     * Paint the circle at the center of the panel
     * @param g
     */
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(color);
        int x = getWidth() / 2 - RADIUS / 2;
        int y = getHeight() / 2 - RADIUS / 2;
        g2.fillOval(x, y, RADIUS, RADIUS);
    }

    /**
     * Reset to default color
     */
    public void resetColor() {
        color = DEFAULT_COLOR;
        refreshColor();
    }

    /**
     * Handle colors array as cyclic array
     */
    public void nextPlayColor() {
        playIndex += 1;
        playIndex = playIndex >= PLAY_COLORS.length ? 0 : playIndex;
        color = PLAY_COLORS[playIndex];
        refreshColor();
    }

    private void refreshColor() {
        revalidate();
        repaint();
    }

}
