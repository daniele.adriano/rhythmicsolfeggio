package com.adrianoda.rhythmicsolfeggio.model;

public class Note {

    public enum Type {
        NORMAL,
        REST
    }

    public enum State {
        UNCHECKED,
        ENTERED,
        SUCCESS,
        ERROR
    }

    private Type type;
    private double duration; // Expressed in BPM fractions
    private Integer bar;     // Number of bar the note belongs to

    // Process 
    private transient State state = State.UNCHECKED;
    private transient Long windowStartTime;
    private transient Long windowEndTime;
    private transient Long absoluteDuration;    // Note absolute duration in millis
    private transient Integer matchPercentage;  // Played note match percentage
    private transient Boolean playing = false;

    public Note() {
    }

    public Note(Type type, double duration) {
        this.type = type;
        this.duration = duration;
    }

    public Note(Type type, double duration, Integer bar) {
        this.type = type;
        this.duration = duration;
        this.bar = bar;
    }

    public void unchecked() {
        this.state = State.UNCHECKED;
    }

    public void entered() {
        this.state = State.ENTERED;
    }

    public void success() {
        this.state = State.SUCCESS;
    }

    public void error() {
        this.state = State.ERROR;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Long getWindowStartTime() {
        return windowStartTime;
    }

    public void setWindowStartTime(Long windowStartTime) {
        this.windowStartTime = windowStartTime;
    }

    public Long getWindowEndTime() {
        return windowEndTime;
    }

    public void setWindowEndTime(Long windowEndTime) {
        this.windowEndTime = windowEndTime;
    }

    public Long getAbsoluteDuration() {
        return absoluteDuration;
    }

    public void setAbsoluteDuration(Long absoluteDuration) {
        this.absoluteDuration = absoluteDuration;
    }

    public Integer getMatchPercentage() {
        return matchPercentage;
    }

    public void setMatchPercentage(Integer matchPercentage) {
        this.matchPercentage = matchPercentage;
    }

    public Integer getBar() {
        return bar;
    }

    public void setBar(Integer bar) {
        this.bar = bar;
    }

    public Boolean getPlaying() {
        return playing;
    }

    public void setPlaying(Boolean playing) {
        this.playing = playing;
    }

    @Override
    public String toString() {
        return "Note{" +
                "type=" + type +
                ", duration=" + duration +
                ", bar=" + bar +
                ", state=" + state +
                ", windowStartTime=" + windowStartTime +
                ", windowEndTime=" + windowEndTime +
                ", absoluteDuration=" + absoluteDuration +
                ", matchPercentage=" + matchPercentage +
                '}';
    }
}
