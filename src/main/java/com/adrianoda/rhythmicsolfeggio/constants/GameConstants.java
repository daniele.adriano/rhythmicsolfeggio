package com.adrianoda.rhythmicsolfeggio.constants;

import jm.constants.DrumMap;
import jm.constants.Durations;
import jm.constants.Instruments;
import jm.constants.Pitches;

public class GameConstants {

    private GameConstants() {
    }

    // GameContext Generation
    public final static Integer DEFAULT_VOLUME = 127;
    public final static Integer STAGE_PER_LEVEL = 10;
    public final static Integer BAR_PER_STAGE = 3;

    // Playing note tolerance
    public final static Double PLAYING_NOTE_TOLERANCE = Durations.DSQ;

    // JMusic constants
    public final static String SOLFEGGIO_SCORE_NAME = "Solfeggio";
    public final static String METRONOMO_PART_NAME = "Metronomo";
    public final static String VOICE_PART_NAME = "Voice";

    public final static Integer VOICE_PITCH = Pitches.A4;
    public final static Integer VOICE_INSTRUMENT = Instruments.ORGAN;
    public final static Integer METRONOMO_PITCH = DrumMap.OPEN_TRIANGLE;
    public final static Integer METRONOMO_INSTRUMENT = Instruments.DRUM;

}
