package com.adrianoda.rhythmicsolfeggio;

import com.adrianoda.rhythmicsolfeggio.model.GameContext;
import com.adrianoda.rhythmicsolfeggio.ui.MainWindow;
import com.adrianoda.rhythmicsolfeggio.utils.GameContextHandler;
import jm.music.data.Score;
import jm.util.Play;

public class GameLauncher {

    public static void main(String[] args) {
        // Used to setup jmusic in order to avoid latency on first ingame play or demo invocation
        Play.midi(new Score());

        // Setup game context and start UI
        GameContext gameContext = GameContextHandler.load();
        java.awt.EventQueue.invokeLater(() -> {
            new MainWindow(gameContext);
        });

    }

}
