package com.adrianoda.rhythmicsolfeggio.ui.component;

import com.adrianoda.rhythmicsolfeggio.model.Note;
import com.adrianoda.rhythmicsolfeggio.model.Stage;
import jm.constants.Durations;

import javax.swing.*;
import java.awt.*;

import static com.adrianoda.rhythmicsolfeggio.utils.MusicSheetMessage.*;

public class MusicSheetTextPane extends JPanel {

    private final static Integer FONT_SIZE = 26;

    private final static Integer NOTE_SPACE = FONT_SIZE;
    private final static Integer SPACE = FONT_SIZE / 2;

    private final static Integer START_X = 10;
    private final static Integer START_Y = 45;

    private Stage stage;

    public MusicSheetTextPane() {
        setBackground(Color.white);
    }

    public void paintComponent(Graphics g) {
        if (stage == null) {
            return;
        }
        Graphics2D g2 = (Graphics2D) g;

        // Setup antialiasing
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHints(rh);

        // Render music sheet
        renderStage(stage, g);
    }

    public void showStage(Stage stage) {
        this.stage = stage;
        revalidate();
        repaint();
    }

    private void renderStage(Stage stage, Graphics g2) {
        g2.setFont(new Font(FONT_NAME, Font.PLAIN, FONT_SIZE));

        Integer x = START_X,        // x for notes, vertical bars and time indication
                xl,                 // x for horizontal line
                yt = START_Y + 2,   // y for time indication (eg. 3/4)
                yn = START_Y + 2,   // y for notes
                yb = START_Y + 15;  // y for vertical bars

        // Time indication
        Integer nUnits = stage.getMetre().getnUnits();
        g2.drawString(getMessage(nUnits), x, yt);
        x += SPACE;
        g2.drawString(SLASH, x, yt);
        x += SPACE;
        g2.drawString(getMessage(stage.getMetre().getMeasUnits()), x, yt);
        x += SPACE + SPACE / 2;

        // Start vertical bar
        g2.drawString(START_BAR, x, yb);
        xl = x + FONT_SIZE / 5;
        x += NOTE_SPACE;

        // Background horizontal line
        // Number of notes + Number or bars (*2 because each symbol is followed by a space)
        Integer nlines = stage.getNotes().size() * 2 + (stage.getNotes().get(stage.getNotes().size() - 1).getBar() * 2);
        g2.setColor(Color.LIGHT_GRAY);
        for (int i = 0; i < nlines; i++) {
            g2.drawString(LINE, xl, yb);
            xl += SPACE;
        }
        g2.setColor(Color.BLACK);

        // Notes
        Integer currentBar = 0;
        Integer tripletCounter = 0;
        for (Note n : stage.getNotes()) {
            double nDuration = n.getDuration();

            // Check and close current bar
            if (n.getBar() != currentBar) {
                g2.drawString(BAR, x, yb);
                x += SPACE;
                currentBar = n.getBar();
            }

            // Start triplet characters
            if (nDuration == Durations.QT || nDuration == Durations.SQT) {
                if (tripletCounter == 0) {
                    g2.drawString(START_TRIPLET, x, yn);
                } else if (tripletCounter == 1) {
                    g2.drawString(CENTER_TRIPLET, x + NOTE_SPACE / 3, yn);
                }
                tripletCounter += 1;
            }

            // Note or rest
            if (n.getPlaying()) {
                g2.setColor(Color.RED);
            }
            g2.drawString(getMessage(n), x, yn);
            g2.setColor(Color.BLACK);

            // End triplet characters
            if (tripletCounter == 3) {
                g2.drawString(END_TRIPLET, x + NOTE_SPACE / 2, yn);
                tripletCounter = 0;
            }
            x += NOTE_SPACE;
        }

        // End bar
        g2.setColor(Color.BLACK);
        g2.drawString(END_BAR, xl, yb);
    }

}
