package com.adrianoda.rhythmicsolfeggio.utils;

import com.adrianoda.rhythmicsolfeggio.model.Note;

import java.util.List;
import java.util.stream.Collectors;

public class NoteCollisionDetector {

    private Long bigBangTime;
    private Long previousTime;
    private List<Note> notes;

    public NoteCollisionDetector(Long bigBangTime, List<Note> notes) {
        this.bigBangTime = bigBangTime;
        this.notes = notes;
        reset();
    }

    /**
     * Check if release time and press time are included inside a specific note window.
     * On the other side press or release should not be included inside RESTs windows.
     *
     * @param pressed to specify press or release action
     */
    public void detect(Boolean pressed) {
        // Time is relative to big bang time
        Long currentTime = System.currentTimeMillis() - bigBangTime;

        // Extract notes which window includes current time
        List<Note> candidates = notes.stream()
                .filter(n -> n.getWindowStartTime() <= currentTime && n.getWindowEndTime() >= currentTime)
                .collect(Collectors.toList());

        // For each note check and set status
        for (Note n : candidates) {
            // Handle pause notes
            if (n.getType() == Note.Type.REST) {
                // Pause note window is reduced so press or release inside pause note window is an error
                n.error();
                n.setMatchPercentage(0);
                continue;
            }

            // Handle normal notes
            if (pressed) {
                if (n.getState() == Note.State.UNCHECKED && n.getAbsoluteDuration() <= 250) {
                    // UNCHECKED -> SUCCESS this is "cheat" to make the game more playable 
                    n.success();
                    n.setMatchPercentage(100);
                } else if (n.getState() == Note.State.UNCHECKED) {
                    // UNCHECKED -> ENTERED
                    n.entered();
                    previousTime = currentTime;
                } else if (n.getState() == Note.State.SUCCESS) {
                    // ENTERED -> ERROR
                    // Occurs when press-release-press inside the same note window
                    n.error();
                    n.setMatchPercentage(0);
                }
            } else {
                // ENTERED -> SUCCESS
                if (n.getState() == Note.State.ENTERED) {
                    Long pressTime = currentTime - previousTime;
                    Long pressPercentage = 100 * pressTime / n.getAbsoluteDuration();
                    n.setMatchPercentage(pressPercentage.intValue());
                    n.success();
                }
            }
        }
    }

    /**
     * Reset note status and note match percentage
     */
    private void reset() {
        notes.forEach(n -> {
            n.unchecked();
            if (n.getType() == Note.Type.NORMAL) {
                n.setMatchPercentage(0);
            } else {
                n.success();
                n.setMatchPercentage(100);
            }
        });
    }
}
