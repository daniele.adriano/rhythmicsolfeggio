package com.adrianoda.rhythmicsolfeggio.utils;

import com.adrianoda.rhythmicsolfeggio.model.Note;
import jm.constants.Durations;

import java.util.HashMap;
import java.util.Map;

public class MusicSheetMessage {
    
    public final static String FONT_NAME = "Bravura";

    public final static String LINE = "\uE010";
    public final static String BAR = "\uE030";
    public final static String START_BAR = "\uE033";
    public final static String END_BAR = "\uE032";
    public final static String SLASH = "\uE08E";
    public final static String DOT = "\uE1E7";
    public final static String START_TRIPLET = "\uE201";
    public final static String CENTER_TRIPLET = "\uE202";
    public final static String END_TRIPLET = "\uE203";

    private final static Map<Integer, String> numbers;
    private final static Map<Double, String> singleNotes;
    private final static Map<Double, String> rests;

    static {
        numbers = new HashMap<>();
        numbers.put(2, "\uE082");
        numbers.put(3, "\uE083");
        numbers.put(4, "\uE084");

        singleNotes = new HashMap<>();
        singleNotes.put(Durations.SB, "\uE1D2");
        singleNotes.put(Durations.DM, "\uE1D3 " + DOT);
        singleNotes.put(Durations.M, "\uE1D3");
        singleNotes.put(Durations.CD, "\uE1D5 " + DOT);
        singleNotes.put(Durations.CT, "\uE1D5");
        singleNotes.put(Durations.C, "\uE1D5");
        singleNotes.put(Durations.QD, "\uE1D7 " + DOT);
        singleNotes.put(Durations.QT, "\uE1D7");
        singleNotes.put(Durations.Q, "\uE1D7");
        singleNotes.put(Durations.SQT, "\uE1D9");
        singleNotes.put(Durations.SQ, "\uE1D9");

        rests = new HashMap<>();
        rests.put(Durations.SB, "\uE4F4");
        rests.put(Durations.DM, "\uE4F5  " + DOT);
        rests.put(Durations.M, "\uE4F5");
        rests.put(Durations.CD, "\uE4E5" + DOT);
        rests.put(Durations.C, "\uE4E5");
        rests.put(Durations.QD, "\uE4E6" + DOT);
        rests.put(Durations.QT, "\uE4E6");
        rests.put(Durations.Q, "\uE4E6");
        rests.put(Durations.SQT, "\uE4E7");
        rests.put(Durations.SQ, "\uE4E7");
    }

    private MusicSheetMessage() {
    }

    public static String getMessage(Note note) {
        String text;
        if (note.getType() == Note.Type.REST) {
            text = rests.get(note.getDuration());
        } else {
            text = singleNotes.get(note.getDuration());
        }
        return text;
    }

    public static String getMessage(Integer number) {
        return numbers.get(number);
    }

}
