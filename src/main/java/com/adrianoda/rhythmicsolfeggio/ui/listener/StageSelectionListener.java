package com.adrianoda.rhythmicsolfeggio.ui.listener;

import com.adrianoda.rhythmicsolfeggio.model.Stage;
import com.adrianoda.rhythmicsolfeggio.ui.component.MusicSheetTextPane;
import com.adrianoda.rhythmicsolfeggio.ui.component.StageButton;
import com.adrianoda.rhythmicsolfeggio.ui.component.StageInfo;
import com.adrianoda.rhythmicsolfeggio.ui.component.StageList;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import static com.adrianoda.rhythmicsolfeggio.ui.constants.TextConstants.BPM_TEXT;

public class StageSelectionListener implements ListSelectionListener {

    private StageInfo stageInfo;
    private JLabel bpmLabel;
    private MusicSheetTextPane musicSheetTextPane;
    private StageButton demoButton;
    private StageButton playButton;

    public StageSelectionListener(StageInfo stageInfo, JLabel bpmLabel, MusicSheetTextPane musicSheetTextPane, StageButton demoButton, StageButton playButton) {
        this.stageInfo = stageInfo;
        this.bpmLabel = bpmLabel;
        this.musicSheetTextPane = musicSheetTextPane;
        this.demoButton = demoButton;
        this.playButton = playButton;
    }

    @Override
    public void valueChanged(ListSelectionEvent event) {
        StageList stageList = (StageList) event.getSource();
        Stage stage = stageList.getSelectedValue();

        playButton.setEnabled(stage.getPlayable());
        playButton.setSelectedStage(stage);
        demoButton.setEnabled(true);
        demoButton.setSelectedStage(stage);

        bpmLabel.setText(BPM_TEXT + stage.getBpm());
        musicSheetTextPane.showStage(stage);
        stageInfo.update(stage);
    }

}
