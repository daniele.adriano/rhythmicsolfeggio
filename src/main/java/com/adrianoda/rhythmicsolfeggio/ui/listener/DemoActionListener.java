package com.adrianoda.rhythmicsolfeggio.ui.listener;

import com.adrianoda.rhythmicsolfeggio.ui.component.CirclePanel;
import com.adrianoda.rhythmicsolfeggio.ui.component.MusicSheetTextPane;
import com.adrianoda.rhythmicsolfeggio.ui.component.StageButton;
import com.adrianoda.rhythmicsolfeggio.ui.component.StageList;
import com.adrianoda.rhythmicsolfeggio.ui.worker.AbstractPlayerWorker;
import jm.util.Play;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.adrianoda.rhythmicsolfeggio.ui.constants.TextConstants.*;

public class DemoActionListener implements ActionListener {

    private StageButton demoButton;
    private StageButton playButton;
    private StageList stageList;
    private CirclePanel circlePanel;
    private MusicSheetTextPane musicSheetTextPane;
    private DemoWorker demoWorker;

    public DemoActionListener(StageButton demoButton, StageButton playButton, StageList stageList, CirclePanel circlePanel, MusicSheetTextPane musicSheetTextPane) {
        this.circlePanel = circlePanel;
        this.musicSheetTextPane = musicSheetTextPane;
        this.demoButton = demoButton;
        this.playButton = playButton;
        this.stageList = stageList;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (demoButton.getText().equals(BUTTON_DEMO_START)) {
            demoWorker = new DemoWorker(demoButton, playButton, stageList, circlePanel, musicSheetTextPane);
            demoWorker.execute();
        } else if (demoButton.getText().equals(BUTTON_DEMO_STOP)) {
            demoWorker.stop();
        }
    }

    protected static class DemoWorker extends AbstractPlayerWorker {

        private StageButton demoButton;
        private StageButton playButton;
        private StageList stageList;

        public DemoWorker(StageButton demoButton, StageButton playButton, StageList stageList, CirclePanel circlePanel, MusicSheetTextPane musicSheetTextPane) {
            super(circlePanel, musicSheetTextPane, playButton.getSelectedStage());
            this.demoButton = demoButton;
            this.playButton = playButton;
            this.stageList = stageList;
        }

        protected void play() {
            Play.midi(demoButton.getSelectedStage().getScore());
        }

        protected void doBefore() {
            demoButton.setText(BUTTON_DEMO_STOP);
            stageList.setEnabled(false);
            playButton.setEnabled(false);
        }

        @Override
        protected void stopping() {
            demoButton.setText(BUTTON_DEMO_STOPPING);
            demoButton.setEnabled(false);
        }

        protected void doAfter() {
            demoButton.setText(BUTTON_DEMO_START);
            stageList.setEnabled(true);
            playButton.setEnabled(true);
            demoButton.setEnabled(true);
        }
    }
}
