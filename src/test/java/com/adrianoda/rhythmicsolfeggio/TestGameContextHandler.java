package com.adrianoda.rhythmicsolfeggio;

import com.adrianoda.rhythmicsolfeggio.model.GameContext;
import com.adrianoda.rhythmicsolfeggio.utils.GameContextHandler;
import jm.util.Play;
import org.junit.Test;

import java.io.InputStreamReader;
import java.io.Reader;

public class TestGameContextHandler {

    private final static String CONFIG_FILE_NAME_LOCAL = "/game-data.json";
    private final static String CONFIG_FILE_NAME_EXT = "/tmp/rhythmicsolfeggio-data.json";

    @Test
    public void play() {
        load();
        GameContext gameContext = GameContextHandler.getInstance();
        Play.midi(gameContext.getStages().get(0).getScore());
    }

    @Test
    public void load() {
        Reader reader = new InputStreamReader(this.getClass().getResourceAsStream(CONFIG_FILE_NAME_LOCAL));
        load(reader);
    }

    @Test
    public void save() {
//        load();
//        try {
//            Writer writer = new FileWriter(CONFIG_FILE_NAME_EXT);
//            GameContextHandler.save(writer);
//        } catch (IOException e) {
//            Assert.fail("Unable to save");
//            return;
//        }
//        try {
//            Reader reader = new FileReader(CONFIG_FILE_NAME_EXT);
//            load(reader);
//        } catch (FileNotFoundException e) {
//            Assert.fail("Unable to load saved file");
//        } catch (IOException e) {
//            Assert.fail("Unable to load saved file");
//        }
    }

    private void load(Reader reader) {
//        GameContext gameContext = GameContextHandler.load(reader);
//        Assert.assertNotNull(gameContext);
    }

}
