package com.adrianoda.rhythmicsolfeggio.model;

public class StageGoal {
    private Integer bestScore;
    private Integer lastScore;

    public StageGoal() {
    }

    public StageGoal(Integer bestScore, Integer lastScore) {
        this.bestScore = bestScore;
        this.lastScore = lastScore;
    }

    public Integer getBestScore() {
        return bestScore;
    }

    public void setBestScore(Integer bestScore) {
        this.bestScore = bestScore;
    }

    public Integer getLastScore() {
        return lastScore;
    }

    public void setLastScore(Integer lastScore) {
        this.lastScore = lastScore;
    }
}