package com.adrianoda.rhythmicsolfeggio.model;

import java.util.List;

public class GameContext {

    private Integer volume;
    private List<Stage> stages;

    public GameContext(Integer volume, List<Stage> stages) {
        this.volume = volume;
        this.stages = stages;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }
}
