package com.adrianoda.rhythmicsolfeggio.ui.constants;

public class TextConstants {

    private TextConstants() {
    }

    public final static String MAIN_WINDOW_TITLE = "Rhythmic Solfeggio";

    public final static String DIFFICULTY_TEXT = "Difficulty: ";
    public final static String BEST_SCORE_TEXT = "Best score: ";
    public final static String LAST_SCORE_TEXT = "Last score: ";
    public final static String BPM_TEXT = "BPM ";
    public final static String SCORE_TEXT = "Score";
    public final static String MATCH_TEXT = "Match";
    public final static String NOTE_TEXT = "Note";

    public final static String BUTTON_START_PLAYING = "Start Playing";
    public final static String BUTTON_PLAY = "Play";
    public final static String BUTTON_DEMO_START = "Demo Start";
    public final static String BUTTON_DEMO_STOP = "Demo Stop";
    public final static String BUTTON_DEMO_STOPPING = "Stopping...";
    public final static String BUTTON_RESET = "Reset";
    public final static String BUTTON_RESETTING = "Resetting...";


}
