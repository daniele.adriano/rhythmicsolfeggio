package com.adrianoda.rhythmicsolfeggio.ui.listener;

import com.adrianoda.rhythmicsolfeggio.ui.component.*;
import com.adrianoda.rhythmicsolfeggio.ui.worker.AbstractPlayerWorker;
import com.adrianoda.rhythmicsolfeggio.utils.NoteCollisionDetector;
import com.adrianoda.rhythmicsolfeggio.utils.StageUtils;
import jm.util.Play;

import java.awt.event.*;

import static com.adrianoda.rhythmicsolfeggio.ui.constants.TextConstants.*;

public class PlayActionListener extends MouseAdapter implements ActionListener, KeyListener {

    private StageButton playButton;
    private StageButton resetButton;
    private StageButton demoButton;
    private StageList stageList;
    private CirclePanel circlePanel;
    private MusicSheetTextPane musicSheetTextPane;
    private StageInfo stageInfo;
    private PlayWorker playWorker;
    private PlayedStageScore playedStageScore;

    public PlayActionListener(StageButton playButton, StageButton resetButton, StageButton demoButton, StageList stageList, StageInfo stageInfo, CirclePanel circlePanel, MusicSheetTextPane musicSheetTextPane, PlayedStageScore playedStageScore) {
        this.playButton = playButton;
        this.resetButton = resetButton;
        this.demoButton = demoButton;
        this.stageList = stageList;
        this.stageInfo = stageInfo;
        this.circlePanel = circlePanel;
        this.playedStageScore = playedStageScore;
        this.musicSheetTextPane = musicSheetTextPane;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (BUTTON_START_PLAYING.equals(event.getActionCommand())) {
            playWorker = new PlayWorker(playButton, resetButton, demoButton, stageList, stageInfo, circlePanel, musicSheetTextPane, playedStageScore);
            playWorker.execute();
        } else if (BUTTON_RESET.equals(event.getActionCommand())) {
            playWorker.stop();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // do nothing
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            pressed();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            released();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        pressed();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        released();
    }

    private void pressed() {
        if (playWorker == null || !playWorker.isPlaying()) {
            return;
        }
        playWorker.handlePlay(true);
    }

    private void released() {
        if (playWorker == null || !playWorker.isPlaying()) {
            return;
        }
        playWorker.handlePlay(false);
    }

    protected static class PlayWorker extends AbstractPlayerWorker {

        private StageButton playButton;
        private StageButton resetButton;
        private StageButton demoButton;
        private StageList stageList;
        private StageInfo stageInfo;
        private PlayedStageScore playedStageScore;

        private Boolean playing = false;
        private Boolean stopRequested = false;

        private NoteCollisionDetector collisionDetector;

        public PlayWorker(StageButton playButton, StageButton resetButton, StageButton demoButton, StageList stageList, StageInfo stageInfo, CirclePanel circlePanel, MusicSheetTextPane musicSheetTextPane, PlayedStageScore playedStageScore) {
            super(circlePanel, musicSheetTextPane, playButton.getSelectedStage());
            this.demoButton = demoButton;
            this.playButton = playButton;
            this.stageList = stageList;
            this.stageInfo = stageInfo;
            this.resetButton = resetButton;
            this.playedStageScore = playedStageScore;
        }

        @Override
        protected void play() {
            collisionDetector = new NoteCollisionDetector(System.currentTimeMillis(), playButton.getSelectedStage().getNotes());
            playing = true;
            Play.midi(StageUtils.getMetronomoPart(playButton.getSelectedStage()));
        }

        protected void doBefore() {
            playButton.setText(BUTTON_PLAY);
            resetButton.setEnabled(true);
            demoButton.setEnabled(false);
            stageList.setEnabled(false);
        }

        @Override
        protected void stopping() {
            stopRequested = true;
            playing = false;
            resetButton.setText(BUTTON_RESETTING);
            resetButton.setEnabled(false);
            playButton.setEnabled(false);
        }

        protected void doAfter() {
            // Required in case doInBackgroud ends normally (no stopping method is invoked)
            playing = false;
            playButton.setEnabled(false);
            resetButton.setEnabled(false);
            playButton.setText(BUTTON_START_PLAYING);
            resetButton.setText(BUTTON_RESET);

            // New thread to let the worker stop without waiting user interaction with message window
            Thread scoreMessageThread = new Thread(() -> {
                StageUtils.updateGoal(playButton.getSelectedStage());
                playedStageScore.showMessage(playButton.getSelectedStage());
                stageInfo.update(playButton.getSelectedStage());
                restoreComponents();
            });

            // Show score window only if game is not resetted
            if (!stopRequested) {
                scoreMessageThread.start();
            } else {
                restoreComponents();
            }
        }

        public Boolean isPlaying() {
            return playing;
        }

        public void handlePlay(Boolean pressed) {
            collisionDetector.detect(pressed);
        }

        private void restoreComponents() {
            // Restore UI components interaction
            playButton.setEnabled(true);
            demoButton.setEnabled(true);
            stageList.setEnabled(true);
            stopRequested = false;
        }
    }
}
