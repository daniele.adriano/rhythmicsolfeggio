package com.adrianoda.rhythmicsolfeggio.ui.worker;

import com.adrianoda.rhythmicsolfeggio.model.Stage;
import com.adrianoda.rhythmicsolfeggio.ui.component.CirclePanel;
import com.adrianoda.rhythmicsolfeggio.ui.component.MusicSheetTextPane;
import jm.util.Play;

import javax.swing.*;

public abstract class AbstractPlayerWorker extends SwingWorker<Void, Void> {

    private CircleBeatWorker beatWorker;
    private NoteColorerWorker noteColorerWorker;

    public AbstractPlayerWorker(CirclePanel circlePanel, MusicSheetTextPane musicSheetTextPane, Stage stage) {
        addPropertyChangeListener(event -> {
            if (event.getPropertyName().equals("state")) {
                switch ((StateValue) event.getNewValue()) {
                    case STARTED:
                        beatWorker = new CircleBeatWorker(circlePanel, stage.getBpm());
                        beatWorker.execute();
                        noteColorerWorker = new NoteColorerWorker(musicSheetTextPane, stage);
                        noteColorerWorker.execute();
                        doBefore();
                        break;
                    case DONE:
                        doAfter();
                        beatWorker.stop();
                        noteColorerWorker.stop();
                        break;
                }
            }
        });
    }

    @Override
    protected Void doInBackground() {
        play();
        return null;
    }

    public void stop() {
        stopping();
        Play.stopMidi();
        // To immediately stop playing thread invoce cancel(true) here
    }

    protected abstract void play();

    protected abstract void doBefore();

    protected abstract void doAfter();

    protected abstract void stopping();
}
