package com.adrianoda.rhythmicsolfeggio.utils;

import com.adrianoda.rhythmicsolfeggio.model.Stage;
import com.adrianoda.rhythmicsolfeggio.model.StageGoal;
import jm.constants.Durations;
import jm.constants.Pitches;
import jm.music.data.Note;
import jm.music.data.Part;
import jm.music.data.Phrase;
import jm.music.data.Score;

import java.util.List;

import static com.adrianoda.rhythmicsolfeggio.constants.GameConstants.*;
import static com.adrianoda.rhythmicsolfeggio.model.Note.State.SUCCESS;

public class StageUtils {

    private StageUtils() {
    }

    /**
     * Setup stage process data like metronomo and voice parts, note time windows
     *
     * @param stage
     * @param volume
     */
    public static void enrich(Stage stage, Integer volume) {
        Integer bpm = stage.getBpm();
        Phrase voicePhrase = new Phrase();
        Double phraseDuration = 0D;
        for (com.adrianoda.rhythmicsolfeggio.model.Note note : stage.getNotes()) {
            // Setup voice phrase 
            Integer pitch = note.getType() == com.adrianoda.rhythmicsolfeggio.model.Note.Type.NORMAL ? VOICE_PITCH : Pitches.REST;
            voicePhrase.add(new jm.music.data.Note(pitch, note.getDuration()));

            // Setup note duration in millis, will be compared to played note duration
            note.setAbsoluteDuration(bpmFractionToMillis(bpm, note.getDuration()));

            // Setup note playing window; a tolerance is used to enlarge/reduce this window in order to make the game more easy to play
            if (com.adrianoda.rhythmicsolfeggio.model.Note.Type.NORMAL == note.getType()) {
                // Normal note window is greater than default window
                note.setWindowStartTime(bpmFractionToMillis(bpm, phraseDuration - PLAYING_NOTE_TOLERANCE));
                note.setWindowEndTime(bpmFractionToMillis(bpm, phraseDuration + note.getDuration() + PLAYING_NOTE_TOLERANCE));
                note.setMatchPercentage(0);
            } else {
                // Pause note window is smaller than default window
                note.setWindowStartTime(bpmFractionToMillis(bpm, phraseDuration + PLAYING_NOTE_TOLERANCE));
                note.setWindowEndTime(bpmFractionToMillis(bpm, phraseDuration + note.getDuration() - PLAYING_NOTE_TOLERANCE));
                note.setMatchPercentage(100);
                note.success();
            }

            // Calculate total phrase duration
            phraseDuration += note.getDuration();
        }

        // Setup metronomo part
        Part metronomoPart = StageUtils.buildMetronomoPart(0, phraseDuration);
        metronomoPart.setDynamic(volume);
        metronomoPart.setTempo(bpm);

        // Setup voice part
        Part voicePart = new Part(voicePhrase, VOICE_PART_NAME, VOICE_INSTRUMENT, 1);
        voicePart.setDynamic(volume);
        voicePart.setTempo(bpm);

        // Setup score with both voice and metronomo parts
        Score score = new Score(SOLFEGGIO_SCORE_NAME);
        score.add(voicePart);
        score.add(metronomoPart);
        stage.setScore(score);
    }

    /**
     * Setup metronomo part
     *
     * @param channel    midi channel
     * @param totalUnits total duration
     * @return metronomo part
     */
    public static Part buildMetronomoPart(Integer channel, Double totalUnits) {
        Phrase metronomo = new Phrase();
        for (int i = 0; i < totalUnits; i++) {
            Note note = new Note(METRONOMO_PITCH, Durations.C);
            metronomo.add(note);
        }
        return new Part(metronomo, METRONOMO_PART_NAME, METRONOMO_INSTRUMENT, channel);
    }

    /**
     * Retrieve metronomo part
     *
     * @param stage
     * @return
     */
    public static Part getMetronomoPart(Stage stage) {
        return stage.getScore().getPart(METRONOMO_PART_NAME);
    }

    /**
     * Retrieve voice part
     *
     * @param stage
     * @return
     */
    public static Part getVoicePart(Stage stage) {
        return stage.getScore().getPart(VOICE_PART_NAME);
    }

    /**
     * Calculate current score depending on note match percentage and update stage goals.
     *
     * @param stage
     */
    public static void updateGoal(Stage stage) {
        // Score = for each note sum 1 / note duration * match percentage
        Integer currentScore = stage.getNotes().stream()
                .filter(n -> n.getState() == SUCCESS)
                .map(n -> 1d / (n.getAbsoluteDuration() / 1000d) * n.getMatchPercentage() / 100d)
                .reduce((s1, s2) -> s1 + s2)
                .map(n -> n * 10)
                .map(n -> n.intValue())
                .orElse(0);
        StageGoal goal = stage.getGoal();
        goal.setLastScore(currentScore);
        if (goal.getBestScore() < currentScore) {
            goal.setBestScore(currentScore);
        }

        // Stage is DONE if at least 50% of the notes are played
        Long nplayed = stage.getNotes().stream().filter(n -> n.getState() == SUCCESS).count();
        Integer nnotes = stage.getNotes().size();
        Long playedPerc = nplayed * 100 / nnotes;
        if (playedPerc >= 50) {
            stage.setDone(true);
        }

        // Update playable flag on all stages
        List<Stage> stages = GameContextHandler.getInstance().getStages();
        for (int i = 1; i < stages.size(); i++) {
            Boolean previousDone = stages.get(i - 1).getDone();
            stages.get(i).setPlayable(previousDone);
        }
    }

    /**
     * Convert duration from BPM fraction to milliseconds
     *
     * @param bpm
     * @param fractionTime
     * @return
     */
    private static Long bpmFractionToMillis(Integer bpm, Double fractionTime) {
        if (fractionTime < 0) {
            return 0l;
        }
        int multiplier = 1000 * 60 / bpm;
        Double millis = fractionTime * multiplier;
        return millis.longValue();
    }
}
