package com.adrianoda.rhythmicsolfeggio;

import com.adrianoda.rhythmicsolfeggio.generator.SimpleStageGenerator;
import com.adrianoda.rhythmicsolfeggio.generator.GameContextGenerator;
import com.adrianoda.rhythmicsolfeggio.model.GameContext;
import com.adrianoda.rhythmicsolfeggio.model.Note;
import com.adrianoda.rhythmicsolfeggio.model.Stage;
import jm.constants.Durations;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestGenerator {

    @Test
    public void testMusicSheetGenerator() {
        Stage stage = SimpleStageGenerator.generate(3, 2);
        printStage(stage);
    }

    @Test
    public void testStageGenerator() {
        GameContext gc =  GameContextGenerator.generate();
        gc.getStages().stream().forEach(this::printStage);
    }

    @Test
    public void testRound() {
        double sum = Durations.CD;
        for (int i = 0; i < 24; i++) {
            sum += Durations.SQT;

            double v = (double) Math.round(sum * 1000) / 1000d;
            System.out.println(sum + " > " + v);
        }
    }

    @Test
    public void testScore() {
        Note n = new Note();
        n.setMatchPercentage(10);
        n.setAbsoluteDuration(250l);

        Note n2 = new Note();
        n2.setMatchPercentage(10);
        n2.setAbsoluteDuration(250l);

        List<Note> notes = new ArrayList<>();
        notes.add(n);
        notes.add(n2);

        double s = 1d / (n.getAbsoluteDuration() / 1000d) * n.getMatchPercentage() / 100d;
        System.out.println(s);
    }

    private void printStage(Stage stage) {
        System.out.println("Generated " + stage.getName() + " - Difficulty " + stage.getDifficulty() + " - Metre " + stage.getMetre());
        for (Note n : stage.getNotes()) {
            System.out.println(n);
        }
    }
}
