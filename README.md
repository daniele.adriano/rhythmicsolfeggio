Beat all stages and become the master of rhythmic solfeggio!
Yes but, how to play?

- Demo Start / Stop: listen the demo of selected stage
- Start Playing: press this button to start the game and to play notes (once clicked you can also use ENTER button to play)
- Reset: stop a started stage

*Note:* Bravura font must be installed in your machine in order to see the music sheet correctly.