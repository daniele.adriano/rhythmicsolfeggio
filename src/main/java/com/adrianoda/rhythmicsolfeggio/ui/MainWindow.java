package com.adrianoda.rhythmicsolfeggio.ui;

import com.adrianoda.rhythmicsolfeggio.model.GameContext;
import com.adrianoda.rhythmicsolfeggio.ui.component.*;
import com.adrianoda.rhythmicsolfeggio.ui.listener.DemoActionListener;
import com.adrianoda.rhythmicsolfeggio.ui.listener.PlayActionListener;
import com.adrianoda.rhythmicsolfeggio.ui.listener.StageSelectionListener;
import com.adrianoda.rhythmicsolfeggio.utils.GameContextHandler;

import javax.swing.*;
import java.awt.*;

import static com.adrianoda.rhythmicsolfeggio.ui.constants.TextConstants.*;

public class MainWindow extends JFrame {

    private final static Integer STAGE_SECTION_HEIGHT = 100;
    private final static Integer BPM_SECTION_HEIGHT = 50;
    private final static Integer MUSIC_SHEET_SECTION_HEIGHT = 100;
    private final static Integer BUTTON_SECTION_HEIGHT = 40;

    private final static Integer WINDOW_WIDTH = 700;
    private final static Integer WINDOW_HEIGHT = 40 + STAGE_SECTION_HEIGHT + BPM_SECTION_HEIGHT + MUSIC_SHEET_SECTION_HEIGHT + BUTTON_SECTION_HEIGHT;

    public MainWindow(GameContext gameContext) throws HeadlessException {
        super(MAIN_WINDOW_TITLE);

        // Stage List and Stage Info
        StageList stageList = new StageList(gameContext.getStages());
        JScrollPane stages = new JScrollPane(stageList);
        StageInfo stageInfo = new StageInfo();
        JPanel stageListAndInfoPanel = new JPanel(new GridLayout(1, 2, 10, 10));
        stageListAndInfoPanel.add(stages);
        stageListAndInfoPanel.add(stageInfo);
        stageListAndInfoPanel.setPreferredSize(new Dimension(WINDOW_WIDTH, STAGE_SECTION_HEIGHT));

        // BPM text and BPM circlePanel
        JLabel bpmLabel = new JLabel();
        CirclePanel circlePanel = new CirclePanel();
        JPanel bpmDataPanel = new JPanel(new GridLayout(1, 3));
        bpmDataPanel.add(bpmLabel);
        bpmDataPanel.add(circlePanel);
        bpmDataPanel.add(Box.createGlue());
        bpmDataPanel.setPreferredSize(new Dimension(WINDOW_WIDTH, BPM_SECTION_HEIGHT));

        // Music sheet section
        MusicSheetTextPane musicSheetTextPane = new MusicSheetTextPane();
        JPanel musicSheet = new JPanel(new BorderLayout());
        musicSheet.add(musicSheetTextPane);
        musicSheet.setPreferredSize(new Dimension(WINDOW_WIDTH, MUSIC_SHEET_SECTION_HEIGHT));
        musicSheet.setBorder(BorderFactory.createEmptyBorder(0, 5, 10, 5));

        // Buttons
        StageButton demoButton = new StageButton(BUTTON_DEMO_START);
        StageButton playButton = new StageButton(BUTTON_START_PLAYING);
        playButton.setEnabled(false);
        StageButton resetButton = new StageButton(BUTTON_RESET);
        resetButton.setEnabled(false);
        JPanel buttons = new JPanel(new BorderLayout());
        buttons.add(demoButton, BorderLayout.LINE_START);
        buttons.add(playButton, BorderLayout.CENTER);
        buttons.add(resetButton, BorderLayout.LINE_END);
        buttons.setPreferredSize(new Dimension(WINDOW_WIDTH, BUTTON_SECTION_HEIGHT));

        // Wrap bpmLabel sections, musicSheetTextPane and buttons
        JPanel bpmAndMusicSheetPanel = new JPanel(new BorderLayout());
        bpmAndMusicSheetPanel.add(bpmDataPanel, BorderLayout.PAGE_START);
        bpmAndMusicSheetPanel.add(musicSheet, BorderLayout.CENTER);
        bpmAndMusicSheetPanel.add(buttons, BorderLayout.PAGE_END);

        // Wrap stage sections and previous wrapped section 
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(stageListAndInfoPanel, BorderLayout.PAGE_START);
        mainPanel.add(bpmAndMusicSheetPanel, BorderLayout.PAGE_END);

        // Add main panel to content pane
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        contentPane.add(mainPanel);

        // Played stage score popup 
        PlayedStageScore playedStageScore = new PlayedStageScore(this);

        
        // Setup listeners
        stageList.addListSelectionListener(new StageSelectionListener(stageInfo, bpmLabel, musicSheetTextPane, demoButton, playButton));
        demoButton.addActionListener(new DemoActionListener(demoButton, playButton, stageList, circlePanel, musicSheetTextPane));
        PlayActionListener actionListener = new PlayActionListener(playButton, resetButton, demoButton, stageList, stageInfo, circlePanel, musicSheetTextPane, playedStageScore);
        playButton.addActionListener(actionListener);
        playButton.addMouseListener(actionListener);
        playButton.addKeyListener(actionListener);
        resetButton.addActionListener(actionListener);
        
        
        // Configure JFrame behaviour, save game context when windows is closed
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                GameContextHandler.save();
                System.exit(0);
            }
        });
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);
        pack();
        setVisible(true);
    }
}
