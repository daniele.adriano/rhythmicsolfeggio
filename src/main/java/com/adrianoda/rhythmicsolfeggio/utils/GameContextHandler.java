package com.adrianoda.rhythmicsolfeggio.utils;

import com.adrianoda.rhythmicsolfeggio.generator.GameContextGenerator;
import com.adrianoda.rhythmicsolfeggio.model.GameContext;
import com.google.gson.Gson;

import java.io.*;

public class GameContextHandler {

    private final static String CONFIG_FILE_NAME = System.getProperty("user.home").concat("/rhythmicsolfeggio-data.json");

    private static GameContext gameContext;
    private static Gson gson = new Gson();

    private GameContextHandler() {
    }

    public static GameContext getInstance() {
        checkNotNull(gameContext);
        return gameContext;
    }

    /**
     * Load existing game context from file. If file is missing then a new game context is created.
     *
     * @return loaded game context
     */
    public static synchronized GameContext load() {
        System.out.println("Loading game context.");
        try {
            Reader reader = new FileReader(CONFIG_FILE_NAME);
            gameContext = gson.fromJson(reader, GameContext.class);
        } catch (FileNotFoundException e) {
            System.out.println("Unable to fine saved game, generating new game context.");
            gameContext = GameContextGenerator.generate();
            save();
        }
        gameContext.getStages().forEach(st -> StageUtils.enrich(st, gameContext.getVolume()));
        return gameContext;
    }


    /**
     * Save loaded game context
     */
    public static synchronized void save() {
        System.out.println("Saving game context...");
        checkNotNull(gameContext);
        try {
            FileWriter writer = new FileWriter(CONFIG_FILE_NAME);
            gson.toJson(gameContext, writer);
            writer.flush();
            System.out.println("Game context saved");
        } catch (IOException e) {
            System.out.println("Unable to save game context...");
            e.printStackTrace();
        }
    }

    private static void checkNotNull(GameContext gameContext) {
        if (gameContext == null) {
            throw new IllegalStateException("Unable to handle gameContext, gameContext is null");
        }
    }

}
