package com.adrianoda.rhythmicsolfeggio.generator;

import com.adrianoda.rhythmicsolfeggio.model.GameContext;
import com.adrianoda.rhythmicsolfeggio.model.Note;
import com.adrianoda.rhythmicsolfeggio.model.Stage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.adrianoda.rhythmicsolfeggio.constants.GameConstants.*;
import static com.adrianoda.rhythmicsolfeggio.generator.SimpleStageGenerator.DIFFICULTY_LEVELS;

public class GameContextGenerator {

    private GameContextGenerator() {
    }

    public static GameContext generate() {
        Map<String, Void> fingerPrints = new HashMap<>();
        List<Stage> stages = new ArrayList<>();
        Integer totalStages = STAGE_PER_LEVEL * DIFFICULTY_LEVELS;
        for (int i = 0; i < totalStages; i++) {
            Integer difficulty = i / STAGE_PER_LEVEL;

            // Do while to regenerate duplicate music sheet
            Stage stage;
            String fingerPrint;
            do {
                stage = SimpleStageGenerator.generate(difficulty, BAR_PER_STAGE);
                fingerPrint = buildFingerPrint(stage.getNotes());
            } while (fingerPrints.containsKey(fingerPrint));
            fingerPrints.put(fingerPrint, null);

            // First stage is playable
            if (i == 0) {
                stage.setPlayable(true);
            }
            stage.setName("Stage " + i);
            stages.add(stage);
        }
        return new GameContext(DEFAULT_VOLUME, stages);
    }

    private static String buildFingerPrint(List<Note> notes) {
        return notes.stream().map(n -> n.getType().name() + n.getDuration()).reduce((n1, n2) -> n1 + n2).get();
    }

}
