package com.adrianoda.rhythmicsolfeggio.model;

import jm.music.data.Score;

import java.util.List;

public class Stage {

    private String name;
    private String difficulty;
    private Integer bpm;
    private StageGoal goal;
    private Metre metre;
    private List<Note> notes;
    private Boolean done = false;       // Is done when played with success
    private Boolean playable = false;   // Is playable if previous stave was done

    private transient Score score;      // Jmusic score


    public Stage() {

    }

    public Stage(Integer bpm, Metre metre, String difficulty, List<Note> notes) {
        this.bpm = bpm;
        this.metre = metre;
        this.difficulty = difficulty;
        this.notes = notes;
        this.goal = new StageGoal(0, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public Integer getBpm() {
        return bpm;
    }

    public void setBpm(Integer bpm) {
        this.bpm = bpm;
    }

    public StageGoal getGoal() {
        return goal;
    }

    public void setGoal(StageGoal goal) {
        this.goal = goal;
    }

    public Metre getMetre() {
        return metre;
    }

    public void setMetre(Metre metre) {
        this.metre = metre;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Boolean getPlayable() {
        return playable;
    }

    public void setPlayable(Boolean playable) {
        this.playable = playable;
    }

    @Override
    public String toString() {
        return "Stage{" +
                "name='" + name + '\'' +
                ", difficulty='" + difficulty + '\'' +
                ", bpm=" + bpm +
                ", goal=" + goal +
                ", metre=" + metre +
                ", notes=" + notes +
                ", score=" + score +
                ", done=" + done +
                '}';
    }
}
