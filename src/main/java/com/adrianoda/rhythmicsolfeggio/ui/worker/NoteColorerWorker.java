package com.adrianoda.rhythmicsolfeggio.ui.worker;

import com.adrianoda.rhythmicsolfeggio.model.Note;
import com.adrianoda.rhythmicsolfeggio.model.Stage;
import com.adrianoda.rhythmicsolfeggio.ui.component.MusicSheetTextPane;

import javax.swing.*;

public class NoteColorerWorker extends SwingWorker<Void, Void> {

    private MusicSheetTextPane musicSheet;
    private Stage stage;
    private Boolean playing = true;

    public NoteColorerWorker(MusicSheetTextPane musicSheet, Stage stage) {
        this.musicSheet = musicSheet;
        this.stage = stage;
        addPropertyChangeListener(event -> {
            if (event.getPropertyName().equals("state")) {
                switch ((StateValue) event.getNewValue()) {
                    case DONE:
                        musicSheet.showStage(stage);
                        break;
                }
            }
        });
    }

    @Override
    protected Void doInBackground() {
        try {
            for (Note n : stage.getNotes()) {
                n.setPlaying(true);
                musicSheet.showStage(stage);
                Thread.sleep(n.getAbsoluteDuration());
                n.setPlaying(false);
                if (!playing) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void stop() {
        playing = false;
    }
}
