package com.adrianoda.rhythmicsolfeggio.model;

public class Metre {
    private Integer nUnits;
    private Integer measUnits;

    public Metre() {
    }

    public Metre(Integer nUnits, Integer measUnits) {
        this.nUnits = nUnits;
        this.measUnits = measUnits;
    }

    public Integer getnUnits() {
        return nUnits;
    }

    public void setnUnits(Integer nUnits) {
        this.nUnits = nUnits;
    }

    public Integer getMeasUnits() {
        return measUnits;
    }

    public void setMeasUnits(Integer measUnits) {
        this.measUnits = measUnits;
    }

    @Override
    public String toString() {
        return "Metre{" +
                "nUnits=" + nUnits +
                ", measUnits=" + measUnits +
                '}';
    }
}