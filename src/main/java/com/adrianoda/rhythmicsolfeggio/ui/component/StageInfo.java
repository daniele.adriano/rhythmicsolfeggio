package com.adrianoda.rhythmicsolfeggio.ui.component;

import com.adrianoda.rhythmicsolfeggio.model.Stage;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import static com.adrianoda.rhythmicsolfeggio.ui.constants.TextConstants.*;

public class StageInfo extends JTextPane {

    public StageInfo() {
        SimpleAttributeSet attribs = new SimpleAttributeSet();
        StyleConstants.setAlignment(attribs, StyleConstants.ALIGN_CENTER);
        StyleConstants.setSpaceAbove(attribs, 5);
        setParagraphAttributes(attribs, true);
        setEditable(false);
    }

    public void update(Stage stage) {
        String stageInfoText = String.join("\n",
                DIFFICULTY_TEXT + stage.getDifficulty(),
                BEST_SCORE_TEXT + stage.getGoal().getBestScore(),
                LAST_SCORE_TEXT + stage.getGoal().getLastScore());
        setText("\n" + stageInfoText);
    }
}
