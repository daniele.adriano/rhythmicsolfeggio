package com.adrianoda.rhythmicsolfeggio.ui.component;

import com.adrianoda.rhythmicsolfeggio.model.Stage;

import javax.swing.*;

public class StageButton extends JButton {

    private Stage selectedStage;

    public StageButton(String label) {
        super(label);
        setEnabled(false);
    }

    public Stage getSelectedStage() {
        return selectedStage;
    }

    public void setSelectedStage(Stage selectedStage) {
        this.selectedStage = selectedStage;
    }
}
