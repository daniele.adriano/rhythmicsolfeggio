package com.adrianoda.rhythmicsolfeggio.ui.component;


import com.adrianoda.rhythmicsolfeggio.model.Note;
import com.adrianoda.rhythmicsolfeggio.model.Stage;
import com.adrianoda.rhythmicsolfeggio.utils.MusicSheetMessage;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import static com.adrianoda.rhythmicsolfeggio.ui.constants.TextConstants.*;
import static com.adrianoda.rhythmicsolfeggio.utils.MusicSheetMessage.FONT_NAME;

public class PlayedStageScore {
    
    private final static Integer WIDTH = 550;
    private final static Integer HEIGHT = 180;

    private JFrame mainFrame;

    public PlayedStageScore(JFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    public void showMessage(Stage stage) {
        // Score
        JLabel score = new JLabel(SCORE_TEXT + ": " + stage.getGoal().getLastScore());

        // Note info
        NoteScoreList nlist = new NoteScoreList(stage.getNotes());
        JScrollPane snlist = new JScrollPane(nlist);
        snlist.setPreferredSize(new Dimension(WIDTH-20, HEIGHT-20));

        // Wrap up
        JPanel panel = new JPanel();
        panel.add(score, BorderLayout.PAGE_START);
        panel.add(snlist, BorderLayout.PAGE_END);
        panel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        JOptionPane.showMessageDialog(mainFrame, panel, SCORE_TEXT, JOptionPane.PLAIN_MESSAGE);
    }

    private class NoteScoreList extends JList<Note> {
        public NoteScoreList(List<Note> notes) {
            // First empty note is used to setup header, see the cell renderer below
            List<Note> listWrap = new ArrayList<>();
            listWrap.add(new Note());
            listWrap.addAll(notes);

            // Setup Jlist
            Vector<Note> vstages = listWrap.stream().collect(Collectors.toCollection(Vector::new));
            setListData(vstages);
            setLayoutOrientation(JList.HORIZONTAL_WRAP);
            setVisibleRowCount(1);
            setCellRenderer((list, note, index, isSelected, cellHasFocus) -> {
                // Each element of the list is a grid with 3 rows and 1 column:
                // row1: played note symbol
                // row2: note duration in BPM
                // row3: played note matching percentage 
                JPanel panel = new JPanel(new GridLayout(3, 1));
                panel.setPreferredSize(new Dimension(50, 150));
                panel.setOpaque(false);

                if (index == 0) {
                    // List header
                    panel.add(new JLabel(" " + NOTE_TEXT));
                    panel.add(new JLabel(" " + BPM_TEXT));
                    panel.add(new JLabel(" " + MATCH_TEXT));
                } else {
                    // List content
                    JLabel noteLabel = new JLabel();
                    noteLabel.setFont(new Font(FONT_NAME, Font.PLAIN, 20));
                    noteLabel.setText(" " + MusicSheetMessage.getMessage(note));
                    double duration = Math.round(note.getDuration() * 100) / 100d;
                    JLabel durationLabel = new JLabel("" + duration);
                    JLabel percentageLabel = new JLabel(note.getMatchPercentage() + "%");
                    panel.add(noteLabel);
                    panel.add(durationLabel);
                    panel.add(percentageLabel);
                }
                return panel;
            });
        }
    }

}
