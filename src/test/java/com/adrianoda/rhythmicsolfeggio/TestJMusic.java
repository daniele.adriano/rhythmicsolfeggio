package com.adrianoda.rhythmicsolfeggio;

import com.adrianoda.rhythmicsolfeggio.utils.StageUtils;
import jm.constants.DrumMap;
import jm.constants.Durations;
import jm.constants.Instruments;
import jm.constants.Pitches;
import jm.music.data.Note;
import jm.music.data.Part;
import jm.music.data.Phrase;
import jm.music.data.Score;
import jm.music.tools.Mod;
import jm.util.Play;
import org.junit.Test;

public class TestJMusic {

    @Test
    public void testMetronomoUtils() {
        Part metronomo = StageUtils.buildMetronomoPart(0, 9D);
        Play.midi(metronomo);
    }

    @Test
    public void playMetronomo() {
        Phrase bpm = new Phrase("BPM");
//        for (int i = 0; i < 20; i++) {
//            bpm.add(new Note(DrumMap.OPEN_TRIANGLE, Durations.C));
//        }
        bpm.add(new Note(DrumMap.OPEN_TRIANGLE, Durations.C));
        Mod.repeat(bpm, 20);
        Part drums = new Part("Metronomo", Instruments.DRUM, 0);
        drums.add(bpm);

        Phrase solf = new Phrase("Solfeggio");
        solf.add(new Note(Pitches.REST, Durations.M));
        solf.add(new Note(Pitches.REST, Durations.M));

        solf.add(new Note(Pitches.G4, Durations.C));
        solf.add(new Note(Pitches.G4, Durations.C));
        solf.add(new Note(Pitches.G4, Durations.C));
        solf.add(new Note(Pitches.REST, Durations.C));

        solf.add(new Note(Pitches.A4, Durations.C));
        solf.add(new Note(Pitches.A4, Durations.C));
        solf.add(new Note(Pitches.A4, Durations.C));
        solf.add(new Note(Pitches.REST, Durations.C));

        solf.add(new Note(Pitches.B4, Durations.C));
        solf.add(new Note(Pitches.B4, Durations.C));
        solf.add(new Note(Pitches.A4, Durations.C));
        solf.add(new Note(Pitches.A4, Durations.C));

        solf.add(new Note(Pitches.G4, Durations.C));
        solf.add(new Note(Pitches.G4, Durations.C));
        solf.add(new Note(Pitches.G4, Durations.C));
        solf.add(new Note(Pitches.REST, Durations.C));

        Part voice = new Part("Voce", Instruments.FLUTE, 1);
        voice.add(solf);

        Score s = new Score();
        s.setTempo(108);
        s.add(drums);
        s.add(voice);
        Play.midi(s);
    }

}
