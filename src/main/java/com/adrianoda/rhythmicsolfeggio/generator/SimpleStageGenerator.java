package com.adrianoda.rhythmicsolfeggio.generator;

import com.adrianoda.rhythmicsolfeggio.model.Metre;
import com.adrianoda.rhythmicsolfeggio.model.Note;
import com.adrianoda.rhythmicsolfeggio.model.Note.Type;
import com.adrianoda.rhythmicsolfeggio.model.Stage;
import jm.constants.Durations;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimpleStageGenerator {

    public final static Integer DIFFICULTY_LEVELS = 4;

    private static Integer[] MIN_BPM_BY_DIFFICULTY = new Integer[]{50, 50, 60, 50};
    private static Integer[] MAX_BPM_BY_DIFFICULTY = new Integer[]{80, 100, 80, 70};
    private static Double[] UNITS_BY_DIFFICULTY = new Double[]{4d, 3d, 2d, 2d};
    private static Double[] SAME_MAX_DURATION_BY_DIFFICULTY = {Durations.SB, Durations.SB, Durations.C, Durations.C};
    private static Double[] DIV_MIN_DURATION_BY_DIFFICULTY = {Durations.C, Durations.Q, Durations.SQ, Durations.SQ};
    private static Double[] DOT_MIN_DURATION_BY_DIFFICULTY = {0d, Durations.CD, Durations.CD, Durations.QD};
    private static Double[] TRIPLET_MIN_DURATION_BY_DIFFICULTY = {0d, 0d, Durations.QT, Durations.SQT};

    private SimpleStageGenerator() {
    }

    public static Stage generate(Integer difficulty, Integer numberOfBars) {
        Double nUnit = UNITS_BY_DIFFICULTY[difficulty];
        List<Note> notes = new ArrayList<>();

        // First bar is a REST
        notes.add(new Note(Type.REST, nUnit, 0));

        // Generate other notes
        for (int i = 1; i < numberOfBars; i++) {
            List<Note> barNotes = randomizeByDifficulty(difficulty, nUnit);
            for (Note n : barNotes) {
                n.setBar(i);
                notes.add(n);
            }
        }

        // Randomize BPM
        Integer minBpm = MIN_BPM_BY_DIFFICULTY[difficulty];
        Integer maxBpm = MAX_BPM_BY_DIFFICULTY[difficulty];
        Integer bpm = roll((maxBpm - minBpm) / 10 + 1) * 10 + minBpm;

        return new Stage(bpm, new Metre(nUnit.intValue(), 4), difficulty.toString(), notes);
    }

    private static List<Note> randomizeByDifficulty(Integer difficulty, Double duration) {
        switch (difficulty) {
            case 0:
                return randomizeDifficulty0(difficulty, duration);
            case 1:
                return randomizeDifficulty1(difficulty, duration);
            case 2:
                return randomizeDifficulty2(difficulty, duration);
            default:
                return randomizeDifficulty3(difficulty, duration);
        }
    }

    private static List<Note> randomizeDifficulty0(Integer difficulty, Double duration) {
        Integer roll = roll100();
        if (roll < 30) {
            return same(difficulty, duration);
        }
        return div(difficulty, duration);
    }

    private static List<Note> randomizeDifficulty1(Integer difficulty, Double duration) {
        Integer roll = roll100();
        if (roll < 30) {
            return same(difficulty, duration);
        }
        return div(difficulty, duration);
    }

    private static List<Note> randomizeDifficulty2(Integer difficulty, Double duration) {
        Integer roll = roll100();
        if (roll < 30) {
            return same(difficulty, duration);
        } else if (roll < 60) {
            return div(difficulty, duration);
        } else if (roll < 80) {
            return dot(difficulty, duration);
        } else {
            return triplet(difficulty, duration);
        }
    }

    private static List<Note> randomizeDifficulty3(Integer difficulty, Double duration) {
        Integer roll = roll100();
        if (roll < 20) {
            return same(difficulty, duration);
        } else if (roll < 50) {
            return div(difficulty, duration);
        } else if (roll < 75) {
            return dot(difficulty, duration);
        } else {
            return triplet(difficulty, duration);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Functions

    /**
     * Leave provided note if its duration is compliant with specified difficulty, otherwise reiterate
     *
     * @param difficulty
     * @param duration
     * @return
     */
    private static List<Note> same(Integer difficulty, Double duration) {
        Double maxDuration = SAME_MAX_DURATION_BY_DIFFICULTY[difficulty];
        if (duration <= maxDuration) {
            return buildList(restOrNote(duration));
        }
        return randomizeByDifficulty(difficulty, duration);
    }

    /**
     * Split the current note if its duration is compliant with specified difficulty and reiterate, otherwise return provided note
     *
     * @param difficulty
     * @param duration
     * @return
     */
    private static List<Note> div(Integer difficulty, Double duration) {
        Double minDuration = DIV_MIN_DURATION_BY_DIFFICULTY[difficulty];
        if (duration <= minDuration) {
            return buildList(restOrNote(duration));
        }
        Double n1Duration;
        Double n2Duration;
        if (duration == Durations.DM) {
            // This to avoid "duina" (two dotted 1/4)
            n1Duration = Durations.M;
            n2Duration = Durations.C;
        } else {
            n1Duration = duration / 2;
            n2Duration = duration / 2;
        }
        List<Note> notes = new ArrayList<>();
        notes.addAll(randomizeByDifficulty(difficulty, n1Duration));
        notes.addAll(randomizeByDifficulty(difficulty, n2Duration));
        return notes;
    }

    /**
     * Create dotted notes if specified difficulty is compliant with specified note, otherwise reiterate
     *
     * @param difficulty
     * @param duration
     * @return
     */
    private static List<Note> dot(Integer difficulty, Double duration) {
        Double minDuration = DOT_MIN_DURATION_BY_DIFFICULTY[difficulty];
        List<Note> notes = new ArrayList<>();
        if (duration == Durations.M && minDuration <= Durations.CD) {
            Note n1 = restOrNote(Durations.CD);
            List<Note> n2 = randomizeByDifficulty(difficulty, Durations.Q);
            addRandomOrder(notes, buildList(n1), n2);
        } else if (duration == Durations.C && minDuration <= Durations.QD) {
            Note n1 = restOrNote(Durations.QD);
            List<Note> n2 = randomizeByDifficulty(difficulty, Durations.SQ);
            addRandomOrder(notes, buildList(n1), n2);
        } else {
            notes = randomizeByDifficulty(difficulty, duration);
        }
        return notes;
    }

    private static void addRandomOrder(List<Note> target, List<Note> n1, List<Note> n2) {
        if (roll100() < 50) {
            target.addAll(n1);
            target.addAll(n2);
        } else {
            target.addAll(n2);
            target.addAll(n1);
        }
    }

    /**
     * Create triplet if specified difficulty is compliant with specified note, otherwise reiterate
     *
     * @param difficulty
     * @param duration
     * @return
     */
    private static List<Note> triplet(Integer difficulty, Double duration) {
        Double minDuration = TRIPLET_MIN_DURATION_BY_DIFFICULTY[difficulty];
        List<Note> notes = new ArrayList<>();
        if (duration == Durations.C && minDuration <= Durations.QT) {
            notes.add(restOrNote(Durations.QT));
            notes.add(restOrNote(Durations.QT));
            notes.add(restOrNote(Durations.QT));
        } else if (duration == Durations.Q && minDuration <= Durations.SQT) {
            notes.add(restOrNote(Durations.SQT));
            notes.add(restOrNote(Durations.SQT));
            notes.add(restOrNote(Durations.SQT));
        } else {
            notes = randomizeByDifficulty(difficulty, duration);
        }
        return notes;
    }

    /**
     * Create rest or note
     *
     * @param duration
     * @return
     */
    private static Note restOrNote(Double duration) {
        Integer rolled = roll100();
        Type type = Type.NORMAL;
        if (rolled < 20) {
            type = Type.REST;
        }
        return new Note(type, duration);
    }

    /**
     * Common method to create list of notes
     *
     * @param notes
     * @return
     */
    private static List<Note> buildList(Note... notes) {
        List<Note> list = new ArrayList<>();
        for (Note n : notes) {
            list.add(n);
        }
        return list;
    }

    /**
     * Roll 100
     *
     * @return
     */
    private static Integer roll100() {
        return roll(100);
    }

    /**
     * Roll
     *
     * @return
     */
    private static Integer roll(Integer maxInt) {
        Random r = new Random();
        return r.nextInt(maxInt);
    }

}
