package com.adrianoda.rhythmicsolfeggio.ui.worker;

import com.adrianoda.rhythmicsolfeggio.ui.component.CirclePanel;

import javax.swing.*;

public class CircleBeatWorker extends SwingWorker<Void, Void> {

    private CirclePanel circlePanel;
    private Integer bpm;
    private Boolean playing = true;

    public CircleBeatWorker(CirclePanel circlePanel, Integer bpm) {
        this.bpm = bpm;
        this.circlePanel = circlePanel;
        addPropertyChangeListener(event -> {
            if (event.getPropertyName().equals("state")) {
                switch ((StateValue) event.getNewValue()) {
                    case DONE:
                        circlePanel.resetColor();
                        break;
                }
            }
        });
    }

    @Override
    protected Void doInBackground() {
        Long sleepTime = Long.valueOf(1000 * 60 / bpm);
        try {
            while (playing) {
                circlePanel.nextPlayColor();
                Thread.sleep(sleepTime / 2);
                circlePanel.nextPlayColor();
                Thread.sleep(sleepTime / 2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void stop() {
        playing = false;
    }
}
