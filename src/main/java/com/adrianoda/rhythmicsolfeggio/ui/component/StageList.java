package com.adrianoda.rhythmicsolfeggio.ui.component;

import com.adrianoda.rhythmicsolfeggio.model.Stage;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class StageList extends JList<Stage> {

    public StageList(List<Stage> stages) {
        Vector<Stage> vstages = stages.stream().collect(Collectors.toCollection(Vector::new));
        setListData(vstages);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setCellRenderer((list, stage, index, isSelected, cellHasFocus) -> {
            JLabel stageLabel = new JLabel(" " + stage.getName());
            stageLabel.setOpaque(true);
            if (stage.getPlayable()) {
                // Playable stages
                if (isSelected) {
                    stageLabel.setBackground(list.getSelectionBackground());
                    stageLabel.setForeground(list.getSelectionForeground());
                } else {
                    stageLabel.setBackground(list.getBackground());
                    stageLabel.setForeground(list.getForeground());
                }
            } else {
                // Not playable stages
                if (isSelected) {
                    stageLabel.setBackground(Color.DARK_GRAY);
                    stageLabel.setForeground(list.getSelectionForeground());
                } else {
                    stageLabel.setBackground(Color.LIGHT_GRAY);
                    stageLabel.setForeground(list.getForeground());
                }
            }
            return stageLabel;
        });
    }

}
